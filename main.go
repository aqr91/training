package main

import (
	_ ""
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/ekomobile/dadata"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"golang.org/x/crypto/bcrypt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

func init() {
	tokenAuth = jwtauth.New("HS256", secret, nil)
}

var secret = []byte("HelpMe;w;")

type User struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

var users = make(map[string]string)

var tokenAuth *jwtauth.JWTAuth

//	@title			HugoProxy API
//	@version		1.0
//	@description	API Server for HugoProxy
//	@host		localhost:8080
//	@BasePath	/
//
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
func main() {
	r := chi.NewRouter()
	proxy := NewReverseProxy("localhost", "1313") // local
	r.Use(proxy.ReverseProxy)
	r.Post("/api/register", registerHandler)
	r.Post("/api/login", loginHandler)
	r.Route("/api/address", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(tokenAuth))
			r.With(jwtauth.Authenticator(tokenAuth)).Post("/search", searchHandler)
			r.With(jwtauth.Authenticator(tokenAuth)).Post("/geocode", GeocodeHandler)
		})
	})
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		log.Fatalln(err)
		return
	}
}

// секьюрити ApiKeyAuth

func UserIfExists(u User) bool {
	if _, ok := users[u.Login]; !ok {
		return false
	} else {
		return true
	}
}

func UserIfValid(u User) bool {
	if u.Login == "" || u.Password == "" {
		return false
	} else {
		return true
	}
}

func InvalidRequest(q int) bool {
	if q == 1 || q == 2 || q == 3 || q == 4 || q == 6 || q == 7 {
		return true
	}
	return false
}

type Address struct {
	Result string `json:"result"`
	Lat    string `json:"lat"`
	Lon    string `json:"lon"`
}

type SearchRequest struct {
	Query string `json:"query"`
}
type SearchResponse struct {
	Addresses []*Address `json:"addresses"`
}

// registerHandler @Summary User registration
//
//	@Tags			Client
//	@Description	This endpoint registers new user
//	@ID				User-Register
//	@Accept			json
//	@Produce		json
//	@Param			input	body		User	true	"Registration request"
//	@Success		200		{object}	string
//	@Router			/api/register [post]
func registerHandler(w http.ResponseWriter, r *http.Request) { // TODO: Работает
	w.Header().Set("Content-Type", "application/json")
	response := User{}
	log.Println("User: ", response)
	err := json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		log.Println("RegisterHandler: \terr := json.NewDecoder(r.Body).Decode(&token)", err)
	}
	if !UserIfValid(response) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login or password is empty"))
		return
	}
	if ok := UserIfExists(response); !ok {
		log.Println("Creating user: ", response)
		encryptedPassword, _ := bcrypt.GenerateFromPassword([]byte(response.Password), bcrypt.DefaultCost)
		user := User{
			Login:    response.Login,
			Password: string(encryptedPassword),
		}
		users[user.Login] = user.Password
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Registration complete. Please sign in!"))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login has been already taken!"))
	}
}

// loginHandler @Summary User Login
//
//	@Tags			Client
//	@Description	This endpoint signs in and generates new JW Token to be used in requests like Search and Geocode endpoints
//	@ID				User-Login
//	@Accept			json
//	@Produce		json
//	@Param			input	body		User	true	"Login request"
//	@Success		200		{string}	string
//	@Router			/api/login [post]
func loginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	response := User{}
	log.Println("User: ", response)
	err := json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		log.Println("RegisterHandler: \terr := json.NewDecoder(r.Body).Decode(&token)", err)
	}
	if !UserIfValid(response) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login or password is empty"))
		return
	}
	if ok := UserIfExists(response); ok {
		log.Println("Verifying password...")
		err := bcrypt.CompareHashAndPassword([]byte(users[response.Login]), []byte(response.Password))
		if err != nil {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("Incorrect password"))
			return
		}
		tokenAuth = jwtauth.New("HS256", secret, nil)
		_, tokenString, err := tokenAuth.Encode(map[string]interface{}{users[response.Login]: "client"})
		if err != nil {
			log.Println("AUTH ENCODE ERR ", err)
		}
		log.Printf("JWTAUTH: %v\n", tokenString)
		w.Write([]byte("Bearer " + tokenString))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("User not exists!"))
	}
}

// searchHandler @Summary Search for addresses
//
//	@Security		Bearer
//	@Tags			DadataAPI
//	@Description	This endpoint searches for addresses via DADATA API
//	@ID				Get-Location
//	@Accept			json
//	@Produce		json
//	@Param			input	body		SearchRequest	true	"Search request"
//	@Success		200		{object}	SearchResponse
//	@Router			/api/address/search [post]
func searchHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var req SearchRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		log.Println(err)
	}

	creds := dadata.Credentials{
		ApiKeyValue:    "5c42a633cfa3348ce695fa2bbb94915fc6d99027",
		SecretKeyValue: "5dbfa037071f8309e1799c33466940844bd3465f",
	}
	api := dadata.NewClient(dadata.WithCredentialProvider(&creds))

	addresses, err := api.CleanAddresses(req.Query)
	if err != nil {
		log.Println("Ошибка в var addresses, err := api.CleanAddresses(req.Query) // searchHandler")
		log.Println(err)
	}
	var convertedAddresses []*Address
	for _, a := range addresses {
		convertedAddresses = append(convertedAddresses, &Address{
			Result: a.Result,
			Lat:    a.GeoLat,
			Lon:    a.GeoLon,
		})
		log.Println(a)
	}
	for _, a := range convertedAddresses { // вывод хороший
		log.Println(a)
	}
	responseData := SearchResponse{Addresses: convertedAddresses}
	err = json.NewEncoder(w).Encode(responseData)
	if err != nil {
		log.Println(err)
	}
}

/*
"lat": "54.823566",
"lon": "36.512334"*/

// GeocodeHandler @Summary Reverse geocode from location to address
//
//	@Security		Bearer
//	@Tags			DadataAPI
//	@Description	This endpoint searches for addresses via DADATA API using location params. Params: 54.823566 36.512334
//	@ID				Get-Address
//	@Accept			json
//	@Produce		json
//	@Param			input	body		GeocodeRequest	true	"Search request"
//	@Success		200		{object}	Address
//	@Router			/api/address/geocode [post]
func GeocodeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	request := GeocodeRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	requestUpd := GeocodeNewRequest{
		Lat: request.Lat,
		Lon: request.Lng,
	}
	if err != nil {
		log.Println(err)
	}
	client := &http.Client{}
	jData, err := json.Marshal(requestUpd) // на всякий
	if err != nil {
		log.Println(err)
	}
	req, err := http.NewRequest("POST", "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", bytes.NewBuffer(jData))
	if err != nil {
		log.Println(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Token 5c42a633cfa3348ce695fa2bbb94915fc6d99027")
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	bodyText, err := io.ReadAll(resp.Body)
	Adresses := Address{}
	json.Unmarshal(bodyText, &Adresses)
	_, err = w.Write(bodyText)
	if err != nil {
		log.Println(err)
	}
}

type GeocodeRequest struct {
	Lat string `json:"lat"`
	Lng string `json:"lng"`
}
type GeocodeNewRequest struct {
	Lat string `json:"lat"`
	Lon string `json:"lon"`
}
type GeocodeResponse struct {
	Addresses []*Address `json:"addresses"`
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("HELLO HANDLER")
	w.WriteHeader(http.StatusOK)
	_, err := fmt.Fprintf(w, "Hello from API")
	log.Println(r.URL.Path)
	if err != nil {
		log.Println(err)
		return
	}
}

type ReverseProxy struct {
	host string
	port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
	return &ReverseProxy{
		host: host,
		port: port,
	}
}

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
    <style>
        body {
            margin: 0;
        }
    </style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/docs/swagger.json",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/api") {
			next.ServeHTTP(w, r)
			return
		}

		if strings.HasPrefix(r.URL.Path, "/docs") {
			http.ServeFile(w, r, "/app/docs/swagger.json") // docker
			return
		}
		if strings.HasPrefix(r.URL.Path, "/swagger") {
			swaggerUI(w, r)
			return
		}
		hugo, err := url.Parse("http://" + rp.host + ":" + rp.port)
		if err != nil {
			log.Fatalln(err)
		}
		httputil.NewSingleHostReverseProxy(hugo).ServeHTTP(w, r)
	})
}
