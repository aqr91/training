FROM golang:alpine
WORKDIR /app
COPY go.mod .
RUN go mod download && go mod tidy
COPY proxy .
RUN go build -o main
EXPOSE 8080
# Запускаем приложение
CMD ["./main"]
